package com.aholake.bnkdemo;

import com.aholake.bnkdemo.service.AccountService;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class IntegrationTestConfiguration {
    @Bean
    public AccountService getAccountService() {
        return new AccountService();
    }
}
