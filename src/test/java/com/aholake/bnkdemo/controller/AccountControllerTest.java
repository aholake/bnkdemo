package com.aholake.bnkdemo.controller;

import com.aholake.bnkdemo.Application;
import com.aholake.bnkdemo.dto.AccountDto;
import com.aholake.bnkdemo.dto.TransactionRequest;
import com.aholake.bnkdemo.model.TransactionType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
@Slf4j
public class AccountControllerTest {
    @Autowired
    private MockMvc mockMvc;

    Consumer<TransactionRequest> transactionRequestConsumer = t -> {
        log.info("Thread {}: Test {} on account {} with amount {}", Thread.currentThread(), t.getTransactionType(), t.getAccountNo(), t.getAmount());
        this.mockMvc.perform(request(HttpMethod.PUT, "/account/doTransaction")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.getObjectAsJson(t)))
                .andExpect(status().isOk());
    };

    @Test
    public void should_create_account_successful() throws Exception {
        String accountNo = "10001";
        this.mockMvc.perform(request(HttpMethod.POST, "/account")
                .param("accountNo", accountNo))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(accountNo)));
    }

    @Test
    public void should_create_account_failed_when_account_no_duplicated() throws Exception {
        String accountNo = "10002";
        this.mockMvc.perform(request(HttpMethod.POST, "/account")
                .param("accountNo", accountNo))
                .andExpect(status().isOk());

        //create twice
        this.mockMvc.perform(request(HttpMethod.POST, "/account")
                .param("accountNo", accountNo))
                .andExpect(status().isBadRequest());
    }


    @Test
    public void should_return_account_json() throws Exception {
        String expectedJson = this.getObjectAsJson(AccountDto.builder()
                .accountNo("0002")
                .balance(BigDecimal.ZERO)
                .build());

        //test on sample data
        this.mockMvc.perform(request(HttpMethod.GET, "/account/0002"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));
    }

    @Test
    public void should_bad_request_on_invalid_account() throws Exception {
        //test on sample data
        this.mockMvc.perform(request(HttpMethod.GET, "/account/00201"))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void should_increase_money_when_deposit() throws Exception {
        //given
        TransactionRequest transactionRequest = TransactionRequest.builder()
                .accountNo("0001")
                .amount(new BigDecimal("10000"))
                .transactionType(TransactionType.DEPOSIT)
                .build();

        AccountDto expectedContent = AccountDto.builder()
                .accountNo("0001")
                .balance(new BigDecimal("10000")).build();

        //when: deposit 10k
        this.mockMvc.perform(request(HttpMethod.PUT, "/account/doTransaction")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.getObjectAsJson(transactionRequest)))
                .andExpect(status().isOk())
                .andExpect(content().json(this.getObjectAsJson(expectedContent)));

        //when: deposit 2k more
        transactionRequest = TransactionRequest.builder()
                .accountNo("0001")
                .amount(new BigDecimal("2000"))
                .transactionType(TransactionType.DEPOSIT)
                .build();
        expectedContent = AccountDto.builder()
                .accountNo("0001")
                .balance(new BigDecimal("12000")).build();

        this.mockMvc.perform(request(HttpMethod.PUT, "/account/doTransaction")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.getObjectAsJson(transactionRequest)))
                .andExpect(status().isOk())
                .andExpect(content().json(this.getObjectAsJson(expectedContent)));
    }

    /**
     * This test we request to do multiple deposit transaction on an same account asynchronously.
     * Our expectation is no data lost and all transaction must be succeeded.
     *
     * @throws Exception
     */
    @Test
    public void do_multi_deposit_transaction_on_an_account_at_the_same_time() throws Exception {
        String testAccount = "10003";
        this.mockMvc.perform(request(HttpMethod.POST, "/account")
                .param("accountNo", testAccount))
                .andExpect(status().isOk());

        //given
        TransactionRequest transactionRequest = TransactionRequest.builder()
                .accountNo(testAccount)
                .amount(new BigDecimal("2000"))
                .transactionType(TransactionType.DEPOSIT)
                .build();

        TransactionRequest transactionRequest1 = transactionRequest.toBuilder()
                .amount(new BigDecimal("1200"))
                .build();

        TransactionRequest transactionRequest2 = transactionRequest.toBuilder()
                .amount(new BigDecimal("1600"))
                .build();

        TransactionRequest transactionRequest3 = transactionRequest.toBuilder()
                .amount(new BigDecimal("100"))
                .build();

        Observable.fromArray(transactionRequest)
                .doOnNext(t -> log.info("Executing transaction 1 on thread {}", Thread.currentThread()))
                .subscribeOn(Schedulers.io())
                .subscribe(transactionRequestConsumer, err -> log.error("Exception", err));

        Observable.fromArray(transactionRequest1)
                .doOnNext(t -> log.info("Executing transaction 2 on thread {}", Thread.currentThread()))
                .subscribeOn(Schedulers.io())
                .subscribe(transactionRequestConsumer, err -> log.error("Exception", err));
        Observable.fromArray(transactionRequest2)
                .doOnNext(t -> log.info("Executing transaction 3 on thread {}", Thread.currentThread()))
                .subscribeOn(Schedulers.io())
                .subscribe(transactionRequestConsumer, err -> log.error("Exception", err));

        Observable.fromArray(transactionRequest3)
                .doOnNext(t -> log.info("Executing transaction 4 on thread {}", Thread.currentThread()))
                .subscribeOn(Schedulers.io())
                .subscribe(transactionRequestConsumer, err -> log.error("Exception", err));

        TimeUnit.SECONDS.sleep(6); //waiting for all processes completed
        this.mockMvc.perform(request(HttpMethod.GET, "/account/" + testAccount))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("balance\":4900.00")));
    }

    @Test
    public void do_multi_deposit_transaction_on_an_account_at_same_time() throws Exception {
        //give
        String testAccount = "30003";
        this.mockMvc.perform(request(HttpMethod.POST, "/account")
                .param("accountNo", testAccount))
                .andExpect(status().isOk());

        //deposit 10000 to test account
        this.mockMvc.perform(request(HttpMethod.PUT, "/account/doTransaction")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.getObjectAsJson(TransactionRequest.builder().accountNo(testAccount).amount(new BigDecimal(10000)).transactionType(TransactionType.DEPOSIT).build())))
                .andExpect(content().string(containsString("balance\":10000.00")))
                .andExpect(content().string(containsString("accountNo\":\"30003\"")))
                .andExpect(status().isOk());

        //successful transaction
        TransactionRequest transactionRequest = TransactionRequest.builder()
                .accountNo(testAccount)
                .amount(new BigDecimal("2000"))
                .transactionType(TransactionType.WITHDRAW)
                .build();

        //successful transaction
        TransactionRequest transactionRequest1 = transactionRequest.toBuilder()
                .amount(new BigDecimal("1200"))
                .build();

        //successful transaction
        TransactionRequest transactionRequest2 = transactionRequest.toBuilder()
                .amount(new BigDecimal("1600"))
                .build();

        //failed transaction
        TransactionRequest transactionRequest3 = transactionRequest.toBuilder()
                .amount(new BigDecimal("1500"))
                .build();

        Observable.fromArray(transactionRequest)
                .doOnNext(t -> log.info("Executing transaction 1 on thread {}", Thread.currentThread()))
                .subscribeOn(Schedulers.io())
                .subscribe(transactionRequestConsumer, err -> log.error("Exception", err));
        Observable.fromArray(transactionRequest1)
                .doOnNext(t -> log.info("Executing transaction 2 on thread {}", Thread.currentThread()))
                .subscribeOn(Schedulers.io())
                .subscribe(transactionRequestConsumer, err -> log.error("Exception", err));
        Observable.fromArray(transactionRequest2)
                .doOnNext(t -> log.info("Executing transaction 3 on thread {}", Thread.currentThread()))
                .subscribeOn(Schedulers.io())
                .subscribe(transactionRequestConsumer, err -> log.error("Exception", err));
        Observable.fromArray(transactionRequest3)
                .doOnNext(t -> log.info("Executing transaction 4 on thread {}", Thread.currentThread()))
                .subscribeOn(Schedulers.io())
                .subscribe(transactionRequestConsumer, err -> log.error("Exception", err));

        TimeUnit.SECONDS.sleep(6); //waiting for all processes completed
        //then
        this.mockMvc.perform(request(HttpMethod.GET, "/account/" + testAccount))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("balance\":3700.00")));

    }

    @Test
    public void should_transfer_money_between_2_account_successfully() throws Exception {
        String testAccount1 = "20004";
        String testAccount2 = "20005";

        //given
        this.mockMvc.perform(request(HttpMethod.POST, "/account")
                .param("accountNo", testAccount1))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("balance\":0")));
        this.mockMvc.perform(request(HttpMethod.POST, "/account")
                .param("accountNo", testAccount2))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("balance\":0")));


        //deposit 10000 to account 1
        this.mockMvc.perform(request(HttpMethod.PUT, "/account/doTransaction")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.getObjectAsJson(TransactionRequest.builder().accountNo(testAccount1).amount(new BigDecimal(10000)).transactionType(TransactionType.DEPOSIT).build())))
                .andExpect(content().string(containsString("balance\":10000.00")))
                .andExpect(status().isOk());

        //when transfer 2000 from acc1 to acc2
        this.mockMvc.perform(request(HttpMethod.PUT, "/account/transferMoney").contentType(MediaType.APPLICATION_JSON).content(this.getObjectAsJson(TransactionRequest
                .builder()
                .accountNo(testAccount1)
                .transactionType(TransactionType.TRANSFER)
                .amount(new BigDecimal(2000)).destinationAccountNo(testAccount2)
                .build())))
                .andExpect(status().isOk());

        //then
        this.mockMvc.perform(request(HttpMethod.GET, "/account/" + testAccount1))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("balance\":8000.00")));
        this.mockMvc.perform(request(HttpMethod.GET, "/account/" + testAccount2))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("balance\":2000.00")));
    }

    @Test
    public void should_transfer_money_between_2_account_failed_when_src_acc_is_not_enough_money() throws Exception {
        String testAccount1 = "10005";
        String testAccount2 = "10006";

        //given
        this.mockMvc.perform(request(HttpMethod.POST, "/account")
                .param("accountNo", testAccount1))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("balance\":0")));
        this.mockMvc.perform(request(HttpMethod.POST, "/account")
                .param("accountNo", testAccount2))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("balance\":0")));


        //deposit 10000 to account 1
        this.mockMvc.perform(request(HttpMethod.PUT, "/account/doTransaction")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.getObjectAsJson(TransactionRequest.builder().accountNo(testAccount1).amount(new BigDecimal(10000)).transactionType(TransactionType.DEPOSIT).build())))
                .andExpect(content().string(containsString("balance\":10000.00")))
                .andExpect(status().isOk());

        //when transfer 2000 from acc1 to acc2
        this.mockMvc.perform(request(HttpMethod.PUT, "/account/transferMoney").contentType(MediaType.APPLICATION_JSON).content(this.getObjectAsJson(TransactionRequest
                .builder()
                .accountNo(testAccount1)
                .transactionType(TransactionType.TRANSFER)
                .amount(new BigDecimal(12000)).destinationAccountNo(testAccount2)
                .build())))
                .andExpect(status().isBadRequest());

        //check balance account 1 and account 2
        this.mockMvc.perform(request(HttpMethod.GET, "/account/" + testAccount1))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("balance\":10000.00")));
        this.mockMvc.perform(request(HttpMethod.GET, "/account/" + testAccount2))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("balance\":0")));
    }

    @Test
    public void should_transfer_money_between_2_account_failed_when_desc_not_found() throws Exception {
        String testAccount1 = "10007";
        String testAccount2 = "10008";

        //given: just create account 1
        this.mockMvc.perform(request(HttpMethod.POST, "/account")
                .param("accountNo", testAccount1))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("balance\":0")));


        //deposit 10000 to account 1
        this.mockMvc.perform(request(HttpMethod.PUT, "/account/doTransaction")
                .contentType(MediaType.APPLICATION_JSON)
                .content(this.getObjectAsJson(TransactionRequest.builder().accountNo(testAccount1).amount(new BigDecimal(10000)).transactionType(TransactionType.DEPOSIT).build())))
                .andExpect(content().string(containsString("balance\":10000.00")))
                .andExpect(status().isOk());

        //when transfer 2000 from acc1 to acc2
        this.mockMvc.perform(request(HttpMethod.PUT, "/account/transferMoney").contentType(MediaType.APPLICATION_JSON).content(this.getObjectAsJson(TransactionRequest
                .builder()
                .accountNo(testAccount1)
                .transactionType(TransactionType.TRANSFER)
                .amount(new BigDecimal(1000)).destinationAccountNo(testAccount2)
                .build())))
                .andExpect(status().isBadRequest());

        //check balance account 1 and account 2
        this.mockMvc.perform(request(HttpMethod.GET, "/account/" + testAccount1))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("balance\":10000.00")));
    }

    private String getObjectAsJson(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            Assert.fail();
            return StringUtils.EMPTY;
        }
    }


}
