package com.aholake.bnkdemo.repository;

import com.aholake.bnkdemo.model.Account;
import com.aholake.bnkdemo.model.AccountBalance;
import com.aholake.bnkdemo.repository.AccountRepository;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountRepositoryTest {
    @Autowired
    private AccountRepository accountRepository;

    private Account testAccount = Account.builder()
            .accountNumber("102000020")
            .accountBalance(AccountBalance.builder()
                    .balance(BigDecimal.ZERO)
                    .build())
            .build();

    @Test
    public void should_persist_account_successfully() {
        //when
        Account persistEntity = this.accountRepository.save(SerializationUtils.clone(this.testAccount));

        //then
        assertThat(this.accountRepository.findAll().size(), is(1));
        assertNotNull(persistEntity.getId());
        assertNotNull(persistEntity.getAccountBalance().getId());
        assertThat(persistEntity.getVersion(), is(0L));
    }

    @Test
    public void should_increase_entity_version_when_update() {
        //given
        Account persistEntity = this.accountRepository.saveAndFlush(SerializationUtils.clone(this.testAccount));
        assertThat(persistEntity.getVersion(), is(0L));

        //when
        Account persistEntity1 = this.accountRepository.saveAndFlush(persistEntity.toBuilder()
                .accountBalance(AccountBalance
                        .builder()
                        .balance(BigDecimal.ONE)
                        .build())
                .build());
        //then
        assertThat(persistEntity1.getAccountBalance().getBalance(), is(BigDecimal.ONE));
        assertThat(persistEntity1.getVersion(), is(1L));
    }

    @Test(expected = ObjectOptimisticLockingFailureException.class)
    public void should_throw_exception_when_use_a_wrong_entity_version() {
        //give
        Account persistEntity = this.accountRepository.saveAndFlush(SerializationUtils.clone(this.testAccount));

        //when
        this.accountRepository.saveAndFlush(persistEntity.toBuilder()
                .accountBalance(AccountBalance
                        .builder()
                        .balance(BigDecimal.ONE)
                        .build())
                .version(2L)
                .build());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void should_throw_exception_when_account_no_is_duplicated() {
        //given
        Account testAccount2 = SerializationUtils.clone(SerializationUtils.clone(testAccount));

        //then
        this.accountRepository.saveAll(Arrays.asList(this.testAccount, testAccount2));
    }
}
