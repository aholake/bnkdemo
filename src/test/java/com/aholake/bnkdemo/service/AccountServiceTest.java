package com.aholake.bnkdemo.service;

import com.aholake.bnkdemo.IntegrationTestConfiguration;
import com.aholake.bnkdemo.dto.TransactionRequest;
import com.aholake.bnkdemo.model.Account;
import com.aholake.bnkdemo.model.AccountBalance;
import com.aholake.bnkdemo.model.TransactionType;
import com.aholake.bnkdemo.repository.AccountRepository;
import com.querydsl.core.types.Predicate;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Optional;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = IntegrationTestConfiguration.class)
public class AccountServiceTest {
    private static final String ACCOUNT_NUMBER = "102000020";
    @Autowired
    private AccountService accountService;

    @MockBean
    private AccountRepository accountRepository;

    @Test
    public void should_do_deposit_transaction_successfully() {
        //given
        Account account = this.buildTestAccount(BigDecimal.ZERO);

        Mockito.when(this.accountRepository.findOne(Mockito.any(Predicate.class))).thenReturn(Optional.of(account));
        AccountBalance accountBalance = account.getAccountBalance();
        Mockito.when(this.accountRepository.saveAndFlush(Mockito.any(Account.class)))
                .thenReturn(account.toBuilder()
                        .accountBalance(accountBalance.toBuilder().balance(new BigDecimal("200000")).build())
                        .build());
        Account result = this.accountService.doTransaction(TransactionRequest.builder().accountNo(ACCOUNT_NUMBER).transactionType(TransactionType.DEPOSIT).amount(new BigDecimal("200000")).build());
        //then
        Assert.assertEquals(result.getAccountBalance().getBalance(), new BigDecimal("200000"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_throw_exception_when_account_not_found() {
        //given
        Mockito.when(this.accountRepository.findOne(Mockito.any(Predicate.class))).thenReturn(Optional.empty());
        this.accountService.doTransaction(TransactionRequest.builder().accountNo(ACCOUNT_NUMBER).transactionType(TransactionType.DEPOSIT).amount(new BigDecimal("200000")).build());
    }

    @Test(expected = ConcurrencyFailureException.class)
    public void should_retry_deposit_and_throw_exception_when_deadlock_occurs_many_times() {
        //given
        Account account = this.buildTestAccount(BigDecimal.ZERO);

        //when
        Mockito.when(this.accountRepository.findOne(Mockito.any(Predicate.class)))
                .thenReturn(Optional.of(account));

        Mockito.when(this.accountRepository.saveAndFlush(Mockito.any(Account.class))).thenThrow(ConcurrencyFailureException.class);
        //then
        this.accountService.doTransaction(TransactionRequest.builder().accountNo(ACCOUNT_NUMBER).transactionType(TransactionType.DEPOSIT).amount(new BigDecimal("200000")).build());
    }

    @Test
    public void should_do_withdraw_successfully() {
        //given: balance 10$
        Mockito.when(this.accountRepository.findOne(Mockito.any(Predicate.class)))
                .thenReturn(Optional.of(this.buildTestAccount(BigDecimal.TEN)));

        TransactionRequest transactionRequest = TransactionRequest.builder().accountNo(ACCOUNT_NUMBER).transactionType(TransactionType.WITHDRAW).amount(new BigDecimal("3")).build();
        //when: withdraw 3$
        Mockito.when(this.accountRepository.saveAndFlush(Mockito.any(Account.class)))
                .thenReturn(this.buildTestAccount(new BigDecimal(7)));

        //then:
        Account account = this.accountService.doTransaction(transactionRequest);
        Assert.assertEquals(account.getAccountBalance().getBalance(), new BigDecimal(7));
    }

    @Test(expected = IllegalArgumentException.class)
    public void should_withdraw_failed_if_balance_is_not_enough() {
        //given: balance 1
        Mockito.when(this.accountRepository.findOne(Mockito.any(Predicate.class)))
                .thenReturn(Optional.of(this.buildTestAccount(BigDecimal.ONE)));

        TransactionRequest transactionRequest = TransactionRequest.builder().accountNo(ACCOUNT_NUMBER).transactionType(TransactionType.WITHDRAW).amount(new BigDecimal("3")).build();
        this.accountService.doTransaction(transactionRequest);
    }

    private Account buildTestAccount(BigDecimal balance) {
        return Account.builder()
                .accountNumber(ACCOUNT_NUMBER)
                .accountBalance(AccountBalance.builder()
                        .balance(balance)
                        .build())
                .build();
    }
}
