package com.aholake.bnkdemo.service;

import com.aholake.bnkdemo.model.base.IEntity;
import com.aholake.bnkdemo.repository.BaseRepository;
import com.google.common.collect.Lists;
import com.querydsl.core.types.Predicate;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;

@Slf4j
public abstract class BaseService<T extends IEntity, ID> {
    protected abstract BaseRepository<T, ID> getBaseRepository();

    public Optional<T> findOne(Predicate predicate) {
        return this.getBaseRepository().findOne(predicate);
    }

    public Optional<T> findOne(ID id) {
        return this.getBaseRepository().findById(id);
    }

    public List<T> findAll(Predicate predicate) {
        return Lists.newArrayList(this.getBaseRepository().findAll(predicate));
    }

    public void delete(T entity) {
        this.getBaseRepository().delete(entity);
    }

    public void deleteById(ID id) {
        this.getBaseRepository().deleteById(id);
    }

    public T save(T entity) {
        return this.getBaseRepository().save(entity);
    }

    public List<T> findAll() {
        return this.getBaseRepository().findAll();
    }

    public long count(Predicate predicate) {
        return this.getBaseRepository().count(predicate);
    }
}
