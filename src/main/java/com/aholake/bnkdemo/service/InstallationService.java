package com.aholake.bnkdemo.service;

import com.aholake.bnkdemo.model.Account;
import com.aholake.bnkdemo.model.AccountBalance;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.Arrays;

/**
 * Initialize some sample data, used for demonstration.
 */
@Service
@AllArgsConstructor
@Slf4j
public class InstallationService {
    private final AccountService accountService;

    @PostConstruct
    public void init() {
        this.createSomeAccounts();
    }

    private void createSomeAccounts() {
        Account account1 = Account.builder()
                .accountNumber("0001")
                .accountBalance(AccountBalance.builder()
                        .balance(BigDecimal.ZERO)
                        .build())
                .build();
        Account account2 = Account.builder()
                .accountNumber("0002")
                .accountBalance(AccountBalance.builder()
                        .balance(BigDecimal.ZERO)
                        .build())
                .build();

        Arrays.asList(account1, account2)
                .forEach(this.accountService::save);
    }
}
