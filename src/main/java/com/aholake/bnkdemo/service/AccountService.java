package com.aholake.bnkdemo.service;

import com.aholake.bnkdemo.dto.TransactionRequest;
import com.aholake.bnkdemo.model.Account;
import com.aholake.bnkdemo.model.AccountBalance;
import com.aholake.bnkdemo.model.QAccount;
import com.aholake.bnkdemo.model.TransactionType;
import com.aholake.bnkdemo.repository.AccountRepository;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

@Service
@Slf4j
public class AccountService extends BaseService<Account, Long> {
    @Getter
    @Autowired
    private AccountRepository baseRepository;

    public Optional<Account> findAccountByAccountNo(String accountNo) {
        return this.findOne(QAccount.account.accountNumber.eq(accountNo));
    }

    /**
     * Add an account.
     * @param accountNo account number, must be unique.
     * @return Account with balance ZERO
     */
    public Account addOne(String accountNo) {
        return this.save(Account.builder()
                .accountNumber(accountNo)
                .accountBalance(AccountBalance
                        .builder()
                        .balance(BigDecimal.ZERO)
                        .build())
                .build());
    }

    /**
     * Transfer money between 2 accounts by calling withdraw on 1st account and deposit on 2nd account.
     * @param transactionRequest transaction request for containing
     *                           account number (src account)
     *                           destination account number,
     *                           transaction type (always be {@link TransactionType TRANSFER},
     *                           amount.
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void transferMoney(@NonNull TransactionRequest transactionRequest) {
        log.info("Withdrawing money from source account...");
        this.doTransaction(TransactionRequest.builder()
                .accountNo(transactionRequest.getAccountNo())
                .transactionType(TransactionType.WITHDRAW)
                .amount(transactionRequest.getAmount())
                .build());

        log.info("Deposit money from destination account...");
        this.doTransaction(TransactionRequest.builder()
                .accountNo(transactionRequest.getDestinationAccountNo())
                .transactionType(TransactionType.DEPOSIT)
                .amount(transactionRequest.getAmount())
                .build());
    }

    /**
     * Do transaction on a account, it includes DEPOSIT and WITHDRAW.
     * If any errors occur, it will be retried 5 times, delay 500miliseconds for each attempt.
     * @param transactionRequest transaction request for containing account number, transaction type and amount.
     * @return account with new state.
     * @throws IllegalStateException if anything goes wrong.
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Retryable(value = {ConcurrencyFailureException.class}, maxAttempts = 5, backoff = @Backoff(delay = 500))
    public Account doTransaction(@NonNull TransactionRequest transactionRequest) {
        Optional<Account> account = Optional.empty();
        TransactionType transactionType = transactionRequest.getTransactionType();
        if (TransactionType.DEPOSIT == transactionType) {
            account = this.deposit(transactionRequest.getAccountNo(), transactionRequest.getAmount());
        }

        if (TransactionType.WITHDRAW == transactionType) {
            account = this.withdraw(transactionRequest.getAccountNo(), transactionRequest.getAmount());
        }

        return account.orElseThrow(() -> {
            log.error("Something is wrong");
            return new IllegalStateException();
        });
    }

    private Optional<Account> deposit(@NonNull String accountNo, @NonNull BigDecimal amount) {
        Account account = this.findAccountByAccountNo(accountNo).orElseThrow(() -> new IllegalArgumentException("No account is found"));
        log.info("Account version before: {}", Optional.of(account).map(Account::getVersion).orElse(-1L));
        AccountBalance accountBalance = Optional.of(account).map(Account::getAccountBalance).orElseThrow(IllegalAccessError::new);
        BigDecimal newBalance = accountBalance.getBalance().add(amount);
        log.info("Old balance {}, changed amount: {}, new balance: {}", accountBalance.getBalance(), amount, newBalance);
        Account savedAccount = this.baseRepository.saveAndFlush(account.toBuilder()
                .accountBalance(accountBalance
                        .toBuilder()
                        .balance(newBalance)
                        .build())
                .build());

        log.info("Deposit successfully, current balance is {}", newBalance);
        log.info("Account version before: {}", Optional.of(savedAccount).map(Account::getVersion).orElse(-1L));

        return Optional.of(savedAccount);
    }

    private Optional<Account> withdraw(@NonNull String accountNo, @NonNull BigDecimal amount) {
        Account account = this.findAccountByAccountNo(accountNo).orElseThrow(() -> new IllegalArgumentException("No account is found"));
        log.info("Account version before: {}", Optional.of(account).map(Account::getVersion).orElse(-1L));

        BigDecimal currentBalance = Optional.of(account).map(Account::getAccountBalance).map(AccountBalance::getBalance).orElse(BigDecimal.ZERO);
        if (amount.compareTo(currentBalance) > 0) {
            log.error("Money in account is not enough for withdrawing");
            throw new IllegalArgumentException("Withdraw amount is exceeded current balance");
        }

        AccountBalance accountBalance = Optional.of(account).map(Account::getAccountBalance).orElseThrow(IllegalStateException::new);
        BigDecimal newBalance = currentBalance.subtract(amount);

        Account savedAccount = this.baseRepository.saveAndFlush(account.toBuilder()
                .accountBalance(accountBalance
                        .toBuilder()
                        .balance(newBalance)
                        .build())
                .build());

        log.info("Account version before: {}", Optional.of(savedAccount).map(Account::getVersion).orElse(-1L));
        log.info("Withdraw successfully, current balance is {}", newBalance);
        return Optional.of(savedAccount);
    }
}
