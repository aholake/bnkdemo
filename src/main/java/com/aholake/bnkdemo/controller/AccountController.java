package com.aholake.bnkdemo.controller;

import com.aholake.bnkdemo.dto.AccountDto;
import com.aholake.bnkdemo.dto.AccountDtoFactory;
import com.aholake.bnkdemo.dto.TransactionRequest;
import com.aholake.bnkdemo.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/account")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @PostMapping
    public AccountDto addAccount(@RequestParam String accountNo) {
        return AccountDtoFactory.getAccountDto(this.accountService.addOne(accountNo));
    }

    @GetMapping("/{accountNo}")
    public AccountDto getAccount(@PathVariable("accountNo") String accountNo) {
        return AccountDtoFactory.getAccountDto(this.accountService.findAccountByAccountNo(accountNo).orElseThrow(() -> new IllegalArgumentException("No account is found")));
    }

    @PutMapping("/doTransaction")
    public AccountDto doDeposit(@RequestBody TransactionRequest transactionRequest) {
        return AccountDtoFactory.getAccountDto(this.accountService.doTransaction(transactionRequest));
    }

    @PutMapping("/transferMoney")
    public void doTransfer(@RequestBody TransactionRequest transactionRequest) {
        this.accountService.transferMoney(transactionRequest);
    }
}
