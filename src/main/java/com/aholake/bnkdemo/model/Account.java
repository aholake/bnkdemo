package com.aholake.bnkdemo.model;

import com.aholake.bnkdemo.model.base.HasMetaData;
import com.aholake.bnkdemo.model.base.HasVersion;
import com.aholake.bnkdemo.model.base.IEntity;
import com.aholake.bnkdemo.model.base.MetaData;
import lombok.*;

import javax.persistence.*;

@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Builder(toBuilder = true)
@ToString
@EqualsAndHashCode
public class Account implements IEntity, HasVersion, HasMetaData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String accountNumber;

    @OneToOne(cascade = CascadeType.ALL)
    @NonNull
    private AccountBalance accountBalance;

    @Version
    private Long version;

    @Embedded
    @Setter
    private MetaData metaData;
}
