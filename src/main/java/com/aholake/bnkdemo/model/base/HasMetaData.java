package com.aholake.bnkdemo.model.base;

public interface HasMetaData {
    MetaData getMetaData();

    void setMetaData(MetaData metaData);
}
