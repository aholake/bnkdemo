package com.aholake.bnkdemo.model.base;

import java.io.Serializable;

public interface IEntity extends Serializable {
    Long getId();
}
