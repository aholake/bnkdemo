package com.aholake.bnkdemo.model.base;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Date;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(toBuilder = true)
@Embeddable
public class MetaData implements Serializable {
    private Date createdDate;
    private Date modifiedDate;
    private String createdUser;
    private String modifiedUser;
}
