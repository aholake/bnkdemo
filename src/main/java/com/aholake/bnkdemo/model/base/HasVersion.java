package com.aholake.bnkdemo.model.base;

public interface HasVersion {
    Long getVersion();
}
