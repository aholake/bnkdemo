package com.aholake.bnkdemo.model;

public enum TransactionType {
    DEPOSIT, WITHDRAW, TRANSFER
}
