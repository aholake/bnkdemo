package com.aholake.bnkdemo.repository;

import com.aholake.bnkdemo.model.Account;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends BaseRepository<Account, Long> {
}
