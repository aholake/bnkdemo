package com.aholake.bnkdemo.repository;

import com.aholake.bnkdemo.model.base.IEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseRepository<T extends IEntity, ID> extends JpaRepository<T, ID>, QuerydslPredicateExecutor<T> {
}
