package com.aholake.bnkdemo.domain.aspect;

import com.aholake.bnkdemo.model.base.HasMetaData;
import com.aholake.bnkdemo.model.base.IEntity;
import com.aholake.bnkdemo.model.base.MetaData;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;

@Aspect
@Component
@Slf4j
public class MetaDataAspect {

    @Around("execution(* com.aholake.bnkdemo.repository.*.save(..)) || execution(* com.aholake.bnkdemo.repository.*.saveAndFlush(..))")
    public Object onSave(ProceedingJoinPoint joinPoint) throws Throwable {
        Object arg = joinPoint.getArgs()[0];
        this.updateMetaData(arg);
        return joinPoint.proceed(new Object[]{arg});
    }

    private void updateMetaData(Object arg) {
        if (arg instanceof HasMetaData && arg instanceof IEntity) {
            log.info("Update meta data");
            IEntity entity = (IEntity) arg;
            HasMetaData withMetaData = (HasMetaData) arg;

            MetaData metaData = Optional.of(withMetaData)
                    .map(HasMetaData::getMetaData)
                    .orElse(MetaData.builder()
                            .createdDate(new Date())
                            .build());
            if (!Objects.isNull(entity.getId())) {
                metaData = metaData.toBuilder()
                        .modifiedDate(new Date())
                        .build();
            }

            withMetaData.setMetaData(metaData);
        }
    }
}
