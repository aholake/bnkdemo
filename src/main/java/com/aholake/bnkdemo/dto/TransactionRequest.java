package com.aholake.bnkdemo.dto;

import com.aholake.bnkdemo.model.TransactionType;
import lombok.*;

import java.math.BigDecimal;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(toBuilder = true)
@ToString
@Getter
public class TransactionRequest {
    private String accountNo;

    private TransactionType transactionType;

    private String destinationAccountNo;

    private BigDecimal amount;
}
