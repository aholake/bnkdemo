package com.aholake.bnkdemo.dto;

import com.aholake.bnkdemo.model.Account;
import com.aholake.bnkdemo.model.AccountBalance;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Optional;

public class AccountDtoFactory {
    public static AccountDto getAccountDto(@NonNull Account account) {
        Optional<@NonNull Account> accountOpt = Optional.of(account);
        return AccountDto.builder()
                .accountNo(accountOpt.map(Account::getAccountNumber).orElse(StringUtils.EMPTY))
                .balance(accountOpt.map(Account::getAccountBalance).map(AccountBalance::getBalance).orElse(BigDecimal.ZERO))
                .build();
    }
}
