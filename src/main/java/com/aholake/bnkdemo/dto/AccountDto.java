package com.aholake.bnkdemo.dto;

import lombok.*;

import java.math.BigDecimal;

@Getter
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class AccountDto {
    private String accountNo;

    private BigDecimal balance;
}
