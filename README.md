# bnkdemo
**Wallet Transaction**

Environment: JDK 8  
Build: `mvn clean install`  
Run: `java -jar target/bnkdemo-1.0-SNAPSHOT.jar` or `mvn spring-boot:run`  

API Collection: https://www.getpostman.com/collections/e7094754d91f2e50dd9c

# Open source libraries:  
- Spring Boot
- H2 Database
- Lombok 
- RxJava (for testing)
- QueryDsl